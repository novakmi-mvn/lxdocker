/* (c) Michal Novák, LxDockerPlugin it.novakmi@gmail.com, see LICENSE file */

package com.github.novakmi.lxdocker

import com.bmuschko.gradle.docker.tasks.image.Dockerfile
import org.gradle.api.InvalidUserDataException
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.slf4j.Logger

import java.util.concurrent.Callable

//CacheableTask
class LxDockerfileTask extends Dockerfile {
    private final static Logger logger = Logging.getLogger(LxDockerfileTask)
    private Map sshKeys = [:]
    private boolean sshRootAccess = false
    private String sshRootAccessPass = null
    private final String defaultCmdSupervisor = "/usr/bin/supervisord"

    /**
     * Gradle property or env. variable for root password (for ssh access)
     */
    final static ROOT_PASS_LX = "ROOT_PASS_LX"
    /**
     * Gradle property or env. variable for root ssh key file (if set to "true", ~/.ssh/id_rsa.pub is used)
     */
    final static INSTALL_SSH_KEY_LX="INSTALL_SSH_KEY_LX"
    /**
     * Gradle property or env. variable for base image (by default ubuntu:20.04)
     */
    final static BASE_IMAGE_TAG_LX="BASE_IMAGE_TAG_LX"
    /**
     * Gradle property or env. variable for DEBIAN_FRONTEND env variable(default is  "noninteractive")
     */
    final static DEBIAN_FRONTEND_LX="DEBIAN_FRONTEND_LX"

    private def installPackages(packageList, skipUpdate = false) {
        logger.info "==> installPackages packageList={}", packageList
        if (packages.size()) {
            if (project.hasProperty("unixType")) {
                logger.info("unixType={}", unixType)
            }
            environmentVariable "DEBIAN_FRONTEND", getProp(DEBIAN_FRONTEND_LX, "noninteractive")
            def packages = packageList.join(" ")
            if (!skipUpdate) {
                runCommand "apt-get update"
            }
            runCommand "apt-get -y install $packages"
        }

        logger.info "<== installPackages"
    }

    /**
     * Helper function to get property value.
     * The property is searched in the following order - gradle property (-Pxxx), system env. variable, defaultVal.
     *
     * @param prop  a String representing property name
     * @param defaultVal an optional default value to return if property is not found
     * @return property value or null (not found and defaultVal not set)
     */
    def getProp(String prop, defaultVal = null) {
        logger.debug "==> getProp prop={} defaultVal={}", prop, defaultVal

        def ret = project.findProperty(prop) ?: System.getenv(prop) ?: defaultVal

        logger.debug "<== getProp ret={}", ret
        return ret
    }

    /**
     * Check if property or env. variable exists
     * @param prop  a String representing property name
     * @return true or false
     */
    private def propOrEnvExists(String prop) {
        logger.debug "==> propOrEnvExists prop={}", prop
        def ret = project.findProperty(prop) != null ? true : System.getenv(prop) != null ? true : false
        logger.debug "<== propOrEnvExists ret={}", ret
        return ret
    }

    /**
     * Helper function to get boolean property value.
     * The property is searched in the following order - gradle property (-Pxxx), system env. variable
     *
     * @param prop  a String representing property name
     * @param defaultVal default value to return if property not set or not boolean
     * @return true if property or env. variable is set to "true" or defVal
     * @see getProp
     * @see getPropValOrTrueFalse
     */
    boolean getPropTrueFalse(String prop, boolean defaultVal = false) {
        logger.debug "==> getPropTrueFalse prop={}", prop

        def ret = defaultVal
        if (propOrEnvExists(prop)) {
            ret = project.findProperty(prop) ?: System.getenv(prop) ?: defaultVal
            ret = ret == "true" ? true : ret != "false" ?: false
        }

        logger.debug "<== getPropTrueFalse ret={}", ret
        return ret
    }

    /**
     * Helper function to get property value or boolean.
     * The property is searched in the following order - gradle property (-Pxxx), system env. variable
     *
     * @param prop  a String representing property name
     * @param defaultVal a  value to return if property not found (default false)
     * @return if property/env. variable is set, it's value is returned, if value is string "true" or "false", corresponding
     *         boolean value si returned. If property or env variable not set, defVal is returned
     * @see getProp
     * @see getPropTrueFalse
     */
    def getPropValOrTrueFalse(String prop, defaultVal=false) {
        logger.debug "==> getPropValOrTrueFalse prop={}", prop

        def ret = defaultVal
        if (propOrEnvExists(prop)) {
            ret = project.findProperty(prop) ?: System.getenv(prop)
            if (ret == "true") ret = true
            if (ret == "false") ret = false
        }

        logger.debug "<== getPropValOrTrueFalse ret={}", ret
        return ret
    }

    // args.rootAccess = true | passwd, sskKeys: true | keys: [key1:[user1, user2], .... ]
    // key1, ... is true or filename string
    /**
     * Configure ssh access. Installs <pre>ssh</pre> package.
     * @param args ... map of arguments, keys:
     *    rootAccess .... enable ssh root access and set password (value, if true, set to  getProp("ROOT_PASS_LX", "lxdocker"))
     *    keys ... sets ssh keys to users, value is map where key is ssh key (path or true), value is  set of users, if key is true, .ssh/id_rsa.pub is used.
     *             if set to null, INSTALL_SSH_KEY_LX property/env. variable is checked, if set for root user
     *
     * <pre>
     * configSsh rootAccess: true, keys: [true : ["root", "tester"] as Set]
     * </pre>
     *
     * @see ROOT_PASS_LX
     * @see INSTALL_SSH_KEY_LX
     */
    def configSsh(Map args) {
        logger.info "==> configSsh args={}", args
        packages += ["ssh"]

        if (args.rootAccess) {
            if (args.rootAccess == true) {
                sshRootAccessPass = getProp(ROOT_PASS_LX, "lxdocker")
            } else {
                sshRootAccessPass = args.rootAccess
            }
            sshRootAccess = true
        }

        if (args.keys == null) {
            def sshKey = getPropValOrTrueFalse(INSTALL_SSH_KEY_LX)
            if (sshKey) {
                args.keys = ["${sshKey}": ["root"] as Set]
            }
        }
        if (args.keys != null) {

            assert args.keys instanceof Map
            //args.keys.each { key, users ->  //TODO gradle exception property sshKeys not found
             for(def entry: args.keys) {
                def key = entry.getKey()
                def users = entry.getValue()
                assert users instanceof Set
                logger.info "key={}, users={}", key, users

                if (key == "true" || key == true) {
                    key = "${System.getProperty('user.home')}/.ssh/id_rsa.pub"
                    logger.info "key={}, users={}", key, users
                }

                key = key.replace("~", System.getProperty('user.home'))
                String fileContents = new java.io.File(key).text
                //users.each { user -> //TODO gradle exception property sshKeys not found
                 for(def user: users) {
                    sshKeys[user] = fileContents.trim()
                }
             }
        }

        logger.info "<== configSsh"
    }

    /**
     * Copies (or links) files from the system to location where docker file is created.
     * This is useful for COPY command as it cannot see files on the system.
     * @param files ... list of files to copy (paths as strings)
     * @param link ... (optional) true if link should be created instead of copying
     *
     * <pre>
     *   copyFilesToContext(["/etc/bash.bashrc"])
     * </pre>
     *
     */
    def copyFilesToContext(files, link = false) {
        logger.info "==> copyFilesToContext"
        def contextDir = new java.io.File(getDestinationDir())
        files.each { file ->
            file = file.replaceFirst("^~", System.getProperty("user.home"));
            logger.debug("File ${file}")
            if (link) {
                def fl = new java.io.File(file)
                def fileName = fl.getName()
                logger.debug("Making link ${contextDir}/${fileName} from ${fl.getCanonicalFile()}")
                ant.symlink(link: "${contextDir}/${fileName}", resource: "${fl.getCanonicalFile()}")
            } else {
                ant.copy(file: file, todir: contextDir)
            }
        }

        logger.info "<== copyFilesToContext"
    }

    /**
     * Tag for Docker file name and its directory location. It can be used by build image tag for Docker image name.
     * According to the tag a directory in  project buildDir is created with corresponding docker file.
     *
     * <pre>
     *   tag = "lxdocker"
     * </pre>
     */
    @Input
    String tag = null
    def setTag(inTag) {
        logger.debug "==> setTag inTag={}", inTag
        tag = inTag
        def destDir = getDestinationDir()
        project.mkdir(destDir)
        def dstFile = project.file("${destDir}/${tag}.dockerfile")
        logger.debug "dstFile={}", dstFile
        destFile.set(dstFile)
        logger.debug "<== setTag tag={}", tag
    }
//
//    def getTag() {
//        return tag
//    }

    /**
     *  It represents image name used for FROM (auto filled if FROM is not specified)
     *  Should be used only as read-only property.
     */
    @Internal
    String fromImage = null

//    String getFromImage() {
//        return fromImage
//    }

    /**
     * Packages to install. Package names should be based on the desired Linux distribution or on Ubuntu/Debian packages.
     * (Planned future functionality is to translate Ubuntu/Debian package names to other distributions if needed.)
     *
     * <pre>
     *   packages = ["mc", "vim"]
     * </pre>
     */
    @Input @Optional
    Set packages = []

    /**
     * Set of users to create in the system.
     *
     * <pre>
     *   users = ["joe", "tester"]
     * </pre>
     *
     * @see userInfo
     */
    @Input @Optional
    Set users = []
    /**
     * Map representing additional user info used when user is created (cannot be applied for "root" and system created users).
     * The key element is username, the value is another map with following (optional)key, value pairs
     *
     * pass ... userpassword (in plaintext)
     * uid .... uid of the user
     * encryptPass ... true|false
     * sshKey .... true|false|path to ssh key file to be installed for the user (if true, the
     *
     * <pre>
     *   userInfo["tester"] = [pass: "testpass", uid: 123, encryptPass: true]
     * </pre>
     *
     * @see users
     */
    @Input @Optional
    Map userInfo = [:]

    /**
     * Closure representing docker instructions to be performed (injected) right before package installation
     *
     * <pre>
     *   instructionsFirst = {
     *        exposePort 8081
     *   }
     * </pre>
     */
    @Internal
    def instructionsFirst = null

    /**
     * Closure representing docker instructions to be performed (injected) right before CMD
     *
     * <pre>
     *   instructionsFirst = {
     *        exposePort 8082
     *    }
     * </pre>
     */
    @Internal
    def instructionsLast = null

    /**
     * List representing Dockerfile CMD instruction (e.g. ["/usr/sbin/sshd", "-D"])
     * First element is executable, the next elements are parameters.
     * Cannot be used together with supervisor
     *
     * <pre>
     *    defaultCmd = ["/usr/sbin/sshd", "-D"]
     * </pre>
     *
     * @see supervisor
     */
    @Input @Optional
    def defaultCmd = null

    /**
     * List representing supervisor (http://supervisord.org/) configuration to start multiple programs.
     * Each item in the list is map with following keys:
     * <br>
     *      program ... name of the supervisor program,
     *      command ... command to be run by supervisor
     * <br>
     * Example to run sshd in supervisor:
     *
     * <pre>
     *     supervisor = [ [program: "sshd", command: "/usr/sbin/sshd -D"] ]
     * </pre>
     *
     * Cannot be used together with defaultCmd
     * @see defaultCmd
     */
    @Input @Optional
    List supervisor = null

    /**
     * Pre-defined set of utils packages ("mc", "vim", "less", "psmisc")
     *
     * <pre>
     *  packages +=utilsPkgs
     * </pre>
     *
     * @see packages
     */
    @Input @Optional
    final Set utilsPkgs = ["mc", "vim", "less", "psmisc"]

    /**
     * Pre-defined set of build packages ("build-essential", "libssl-dev")
     *
     * <pre>
     *  packages +=buildPkgs
     * </pre>
     *
     * @see packages
     */
    @Input @Optional
    final Set buildPkgs = ["build-essential", "libssl-dev"]

    /**
     * Pre-defined set of network packages ("iputils-ping", "traceroute", "tcpdump", "iptables", "nmap", "net-tools")
     *
     * <pre>
     *  packages +=networkPkgs
     * </pre>
     *
     * @see packages
     */
    @Input @Optional
    final Set networkPkgs = ["iputils-ping", "traceroute", "tcpdump", "iptables", "nmap", "net-tools"]

    /**
     * Pre-defined default command list representing launch of sshd (["/usr/sbin/sshd", "-D"]).
     *
     * <pre>
     *  defaultCmd = defaultCmdSsh
     * </pre>
     *
     * @see defaultCmd
     */
    @Input @Optional
    final List defaultCmdSsh = ["/usr/sbin/sshd", "-D"]

    /**
     * Pre-defined supervisor config for launch of sshd
     *
     * <pre>
     *  supervisor = [supervisorSsh]
     * </pre>
     *
     * @see supervisor
     */
    @Input @Optional
    final Map supervisorSsh = [program: "sshd", command: "/usr/sbin/sshd -D"]

    @OutputDirectory
    def getDestinationDir() {
        logger.info "==> getDestDir destTag={}", tag

        assert tag != null
        def destDir = "${project.buildDir}/${tag}"

        logger.info "<== getDestDir ${destDir}"
        return destDir
    }

    private def handleSshKeys(Map sshKeys) {
        logger.debug "==> handleSshKeys"

        if (sshKeys.size()) {
            sshKeys.each { user, key ->
                def homeDir = "/$user"
                if (user != "root") {
                    homeDir = "/home${homeDir}"
                }
                runCommand "mkdir -p ${homeDir}/.ssh"
                runCommand "echo '${key}' >> ${homeDir}/.ssh/authorized_keys"
                if (user != "root") {
                    runCommand "chown -R ${user}:${user} ${homeDir}/.ssh"
                }
            }
        }

        logger.debug "<== handleSshKeys"
    }

    private def handleUsers(Set users) {
        logger.debug "==> handleUsers"

        if (users.size()) {
            users.each { user ->
                def cmd = "useradd -d /home/${user} -m -s /bin/bash"
                Map params = userInfo[user]
                if (params == null) {
                    params = [:]
                    params.pass = user
                }
                if (params.uid != null) {
                    cmd += " -u ${params.uid}"
                }
                if (params.encryptPass) {
                    cmd += " -p \$(openssl passwd -1 ${params.pass})"
                } else {
                    cmd += " -p ${params.pass}"
                }
                cmd += " ${user}"
                runCommand cmd
            }
        }

        logger.debug "<== handleUsers"
    }

    private def handleSshRootAccess() {
        logger.debug "==> setupSshAccess"

        // enable ssh access for development machine    https://docs.docker.com/engine/examples/running_ssh_service/#build-an-eg_sshd-image
        if (sshRootAccess) {
            runCommand "mkdir /var/run/sshd"
            if (sshRootAccessPass) {
                runCommand "echo 'root:${sshRootAccessPass}' | chpasswd" // password confd
                runCommand "sed -i 's/PermitRootLogin \\(prohibit\\|without\\)-password/PermitRootLogin yes/' /etc/ssh/sshd_config"
            }
            runCommand "sed -i 's/#PermitRootLogin/PermitRootLogin/' /etc/ssh/sshd_config"
            runCommand "sed 's@session\\s*required\\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd"
            environmentVariable "NOTVISIBLE", "in users profile"
            runCommand 'echo "export VISIBLE=now" >> /etc/profile'
        }

        logger.debug "<== setupSshAccess"
    }

    private def handleSupervisor(supervisorList) {
        logger.debug "==> handleSupervisor supervisorList={}", supervisorList

        def supervisorConfPath = "/etc/supervisor/conf.d"
        def supervisorConfFile = "${supervisorConfPath}/supervisord.conf"

        runCommand "echo '[supervisord]' >> ${supervisorConfFile}"
        runCommand "echo 'nodaemon=true' >> ${supervisorConfFile}"

        supervisorList.each { s ->
            logger.debug "s.program={} s.command={} s.params={}", s.program, s.command, s.params

            runCommand "echo '' >> ${supervisorConfFile}"
            runCommand "echo '[program:${s.program}]' >> ${supervisorConfFile}"
            if (s.params) {
                s.params.each { p ->
                    runCommand "echo '${p}' >> ${supervisorConfFile}"
                }
            }
            runCommand "echo 'command=${s.command}' >> ${supervisorConfFile}"
        }
        defaultCommand defaultCmdSupervisor

        logger.debug "<== handleSupervisor"
    }

    private def prependFromIfNeeded() {
        logger.debug "==> prependFromIfNeeded"
        def instr = getInstructions().get().collect()
        def fromPos = instr.findIndexOf { it.keyword == FromInstruction.KEYWORD }
        logger.debug("fromPos={}", fromPos)
        if (fromPos < 0) {
            logger.info("From not found, prepending from")
            fromImage = getProp(BASE_IMAGE_TAG_LX, "ubuntu:22.04")
            getInstructions().empty()
            from fromImage
            instr.each {i -> getInstructions().add(i)}
        } else {
            def fromText = instr.get(fromPos).getText()
            fromImage =  fromText.tokenize(' ')[-1]
            logger.info("fromImage={}", fromImage)
        }
        logger.debug "<== prependFromIfNeeded"
    }

    /**
     * Hook function that can be used in derived Task (class) to inject Dockerfile commands
     * right after FROM and instructionsFirst
     * @see instructionsFirst
     */
    protected void taskHookFirst() {
        logger.info "==> taskHookFirst"
        // empty - can override in child task(s)
        logger.info "<== taskHookFirst"
    }

    /**
    * Hook function that can be used in derived Task (class) to inject Dockerfile commands
    * right after packages are installed and before users are created.
    */
    protected void taskHook() {
        logger.info "==> taskHook"
        // empty - can override in child task(s)
        logger.info "<== taskHook"
    }

    /**
     * Hook function that can be used in derived Task (class) to inject Dockerfile commands
     * before instructionsLast
     * @see instructionsLast and defaultCmd or supervisor
     */
    protected void taskHookLast() {
        logger.info "==> taskHookLast"
        // empty - can override in child task(s)
        logger.info "<== taskHookLast"
    }


    @TaskAction
    void create() {
        logger.info "==> create"
        assert tag != null
        prependFromIfNeeded()
        if (instructionsFirst) {
            instructionsFirst()
        }
        taskHookFirst()
        // assemble all packages and install them
        if (sshKeys.size()) {
            packages += "ssh"
        }
        if (supervisor) {
            packages += "supervisor"
        }
        installPackages(packages)
        if ("tcpdump" in packages) {
            //https://github.com/moby/moby/issues/14140
            runCommand "mv /usr/sbin/tcpdump /usr/bin/tcpdump"
        }
        taskHook()
        handleUsers(users)
        handleSshKeys(sshKeys)
        handleSshRootAccess()
        taskHookLast()
        if (instructionsLast) {  // perform last user commands (before default commands)
            instructionsLast()
        }
        if (defaultCmd) {
            if (supervisor != null) {
                throw new InvalidUserDataException("'defaultCmd` and `supervisor` cannot be used simultaneously!")
            }
            defaultCommand(project.provider(new Callable() {
                List call() throws Exception {
                    defaultCmd
                }
            }))
        }
        if (supervisor) {
            handleSupervisor(supervisor)
        }

        super.create()
        logger.info "<== create"
    }
}