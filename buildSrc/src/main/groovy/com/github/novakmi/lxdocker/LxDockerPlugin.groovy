/* (c) Michal Novák, LxDockerPlugin it.novakmi@gmail.com, see LICENSE file */

package com.github.novakmi.lxdocker

import org.gradle.api.Project
import org.gradle.api.Plugin


class LxDockerPlugin implements Plugin<Project> {
    //@Override
    /**
     * Gradle plugin entry point.
     * Registers plugin ` LxDockerfile` of `LxDockerfileTask` type.
     *
     * @param target
     * @see LxDockerfileTask
     */
    void apply(Project target) {
        target.task('LxDockerfile', type: LxDockerfileTask)
    }
}
