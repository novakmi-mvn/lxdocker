/* (c) Michal Novák, LxDockerPlugin it.novakmi@gmail.com, see LICENSE file */

package com.github.novakmi.lxdocker.test

import com.github.novakmi.lxdocker.LxDockerfileTask
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.testng.Assert
import org.testng.annotations.Test

class LxDockerfileTaskStartTest {

    @Test(groups=["basic"])
    public void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        def task = project.task('lxdocker', type: LxDockerfileTask)
        Assert.assertTrue(task instanceof LxDockerfileTask)
    }

}
