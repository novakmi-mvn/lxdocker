/* (c) Michal Novák, LxDockerPlugin it.novakmi@gmail.com, see LICENSE file */

package com.github.novakmi.lxdocker.test

class TestUtils {

     static List getDockerFileLines(testProjectDir, tag) {
         File dockerfile = new File("$testProjectDir/build/${tag}/${tag}.dockerfile")
         def text = dockerfile.text
         List lines = text.split("\n")
         return lines
     }

    static boolean containsInOrder(List lines, content) {
        int contentIndex = 0
        for(String l: lines) {
            if (content.size() == contentIndex) {
                break
            }
            if (l.equals(content[contentIndex])) {
                contentIndex++
            }
        }
        return content.size() == contentIndex
    }

    static boolean contains(List lines, line) {
        return containsInOrder(lines, [line])
    }
}
